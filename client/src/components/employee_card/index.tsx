import React, { FC } from "react";
import { employee, employeeStatus } from "../../models/embloyee";
import { setEmployeesStatus } from "../../services/employees_service";

import "./index.scss";

type Props = {
  data: employee;
  changeStatus: (id:string, data:employee)=> void
};


const EmployeeCard: FC<Props> = ({ data, changeStatus }) => {
  const setStatus = (id: string, data: employee)=>{
    changeStatus(id, data);
  }
  return (
    <li>
      <div className="emb-item">
        <div className="item-name">{data.name}</div>
        <div className="steps-wrapper">
          <ul className="item-steps">
            {employeeStatus.map((item) => {
              return (
                <li key={item}>
                  <button
                    className={`btn step-item ${
                      data.status === item ? "active" : ""
                    }`}
                    onClick={()=>{setStatus(data.id, {...data, status: item})}}
                  >
                    <span className="text">{item}</span>
                  </button>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </li>
  );
};

export default EmployeeCard;
