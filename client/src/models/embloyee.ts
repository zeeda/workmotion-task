export interface employee {
  id: string;
  name: string;
  status: string;
}
export const employeeStatus:Array<string> = [
  "ADDED",
  "IN-CHECK",
  "APPROVED",
  "ACTIVE",
  "INACTIVE",
]
