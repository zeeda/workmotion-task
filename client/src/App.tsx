import React, { FC } from "react";
import { Routes, Route, Link } from "react-router-dom";

import Home from "./pages/home";

const App: FC = () => {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
      </Routes>
    </div>
  );
};

export default App;
