import React, { ReactNode, useEffect, useState } from "react";
import { JsxElement } from "typescript";
import EmployeeCard from "../../components/employee_card";
import { employee } from "../../models/embloyee";
import {
  getEmployees,
  setEmployeesStatus,
} from "../../services/employees_service";
import "./index.scss";
type Props = {};

const Home = (props: Props) => {
  const [employees, setemployees] = useState<Array<employee>>([]);
  useEffect(() => {
    getData();
    return () => {};
  }, []);
  const getData = async () => {
    console.log('Get Employees data!');
    
    let resp = await getEmployees();
    setemployees(resp);
  };

  const handleStatusChange = async (id: string, data: employee) => {
    let response = await setEmployeesStatus(id, data);
    if (response) {
      getData();
    }
  };

  const renderEmbList = (list: employee[]) => {
    return list.map((item: employee) => {
      return (
        <EmployeeCard
          data={item}
          key={item.id}
          changeStatus={handleStatusChange}
        ></EmployeeCard>
      );
    });
  };
  return (
    <main>
      <div className="container">
        <div className="main-title">
          <h1 className="title">employees List</h1>
        </div>
        <div className="list-wrapper">
          <ul>{renderEmbList(employees)}</ul>
        </div>
      </div>
    </main>
  );
};

export default Home;
