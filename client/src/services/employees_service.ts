import axios from "axios";
import { employee } from "../models/embloyee";

export const getEmployees = async () => {
    const list: Array<employee> = [];
    let response = await axios.get('http://localhost:3001/employees');
    console.log(response);
    list.push(...response.data);
    return list;
}

export const setEmployeesStatus = async (id: string, data: employee) => {
    let response:{data:employee};
    response = await axios.patch('http://localhost:3001/employees/'+id, data);
    return response.data;
}